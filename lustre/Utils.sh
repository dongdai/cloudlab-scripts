#!/usr/bin/env bash

if [ "$#" -ne 3 ]; then
    echo "Usage: $0 NUM_SERVER NUM_CLIENT COMMAND [COMPILE, INSTALL, DEPLOY, MOD, UMOUNT, SNTP]" >&2
    exit 1
fi

dir="/proj/dirr-PG0/tools/cloudlab-scripts/lustre/"
srv=`expr $1 - 1`       # all lustre servers (mds and oss)
clt=`expr $2 - 1`       # total number of clients
start=`expr $srv + 1`   # where client starts
end=`expr $start + $clt`  # where client ends

# To setup a new Luster cluster, just call following COMMANDS one by one: COMPILE, INSTALL, MOD, DEPLOY, UMOUNT. 
# COMPILE
if [ "COMPILE" = $3 ]; then
    for i in $(seq 0 $srv)
    do
	    echo COMPILE Server node$i
	    ssh node$i "$dir/CompileLustre.sh" &
    done
    for i in $(seq $start $end)
    do
	    echo COMPILE Client node$i
	    ssh node$i "$dir/CompileLustre.sh" &
    done
fi

# INSTALL
if [ "INSTALL" = $3 ]; then
    for i in $(seq 1 $srv)
    do
	    echo Re-INSTALL server node$i
	    ssh node$i "$dir/ReInstallLustre.sh" &
    done
    for i in $(seq $start $end)
    do
        echo Re-Install client node$i
        ssh node$i "$dir/ReInstallClient.sh"
    done
    $dir/ReInstallLustre.sh
fi

# SETUPMOD
if [ "MOD" = $3 ]; then
    for i in $(seq 0 $end)
    do
	    echo SETUP MOD Node$i
	    ssh node$i "sudo $dir/SetupMod.sh"
    done
fi

# DEPLOY
if [ "DEPLOY" = $3 ]; then
    echo DEPLOY Lustre
    $dir/DeployLustre.sh $1 $2 &
fi

# UMOUNT
if [ "UMOUNT" = $3 ]; then
    for i in $(seq $start $end)
    do
        echo umount client node$i
        ssh node$i "sudo umount /lustre"
    done

    for i in $(seq 1 $srv)
    do
	    echo umount ost node$i
	    ssh node$i "sudo umount /lustre"
    done
    
    echo umount MDS
    sudo umount /lustre
fi

# MOUNT
if [ "MOUNT" = $3 ]; then
    # mount it back
    echo mount MDS
    sudo mount -t lustre /dev/sdb /lustre

    for i in $(seq 1 $srv)
    do
	    echo mount ost node$i
	    ssh node$i "sudo mount -t lustre /dev/sdb /lustre"
    done

    for i in $(seq $start $end)
    do
        echo mount client node$i
        ssh node$i "sudo mount -t lustre 10.10.1.1@tcp0:/lustre /lustre"
    done
fi

# LNET
if [ "LNET" = $3 ]; then
    for i in $(seq 0 $srv)
    do
	    echo Re-Configure LNET Node$i
	    ssh node$i "sudo lnetctl lnet unconfigure"
        ssh node$i "sudo lnetctl lnet configure"
        ssh node$i "sudo depmod -a"
        ssh node$i "sudo modprobe lustre"
    done
fi

# SNTP
if [ "SNTP" = $3 ]; then
    for i in $(seq 0 $end)
    do
	    echo SNTP Node$i
	    ssh node$i "sudo sntp -s 24.56.178.140" &
    done
fi

# LFSCK Performance Test
if [ "LFSCK" = $3 ]; then
    # on MDT
    sudo lctl debug_kernel /tmp/debug.lfsck
    # on OST-1
    ssh node1 "sudo lctl debug_kernel /tmp/debug.lfsck"

    # on MDT
    netstat --interfaces=enp6s0f0 -c 1 > /proj/dirr-PG0/tools/lfsck-mdt-log-network.txt &
    iostat -p sdb -d 1 > /proj/dirr-PG0/tools/lfsck-mdt-log-sdb.txt &
    # on OST-1
    ssh node1 "netstat --interfaces=enp6s0f0 -c 1 > /proj/dirr-PG0/tools/lfsck-ost1-log-network.txt" &
    ssh node1 "iostat -p sdb -d 1 > /proj/dirr-PG0/tools/lfsck-ost1-log-sdb.txt" &

    # start LFSCK from MDT.
    # sudo lctl lfsck_start -M lustre-MDT0000 -A -t all -r
    # sudo lctl lfsck_start -t layout -M lustre-MDT0000
    # sudo lctl lfsck_start -t namespace -M lustre-MDT0000
    # sudo lctl lfsck_start -t namespace -t layout -M lustre-MDT0000
    # sudo lctl lfsck_start -t scrub -M lustre-MDT0000
    # sudo lctl lfsck_query -M lustre-MDT0000
    # /proj/dirr-PG0/tools/mdt-log.txt
    # /proj/dirr-PG0/tools/ost1-log.txt
fi

# STOP Resource Monitoring Script
if [ "KILL" = $3 ]; then
    # on MDT
    killall netstat
    killall iostat

    # on OST
    ssh node1 "killall netstat"
    ssh node1 "killall iostat"

    # on MDT
    sudo lctl debug_kernel /tmp/debug.lfsck
    sudo chown dirruncc:dirr-PG0 /tmp/debug.lfsck
    cp /tmp/debug.lfsck /proj/dirr-PG0/tools/lfsck-mdt-debug-lfsck.txt
    # on OST-1
    ssh node1 "sudo lctl debug_kernel /tmp/debug.lfsck"
    ssh node1 "sudo sudo chown dirruncc:dirr-PG0 /tmp/debug.lfsck"
    ssh node1 "cp /tmp/debug.lfsck /proj/dirr-PG0/tools/lfsck-ost1-debug-lfsck.txt"
fi

# AGING
if [ "AGING" = $3 ]; then
    index=0
    for i in $(seq $start $end)
    do
        echo run aging on client node$i
        ssh node$i "/proj/dirr-PG0/tools/cloudlab-scripts/lustre/aging.py 100920 $index 9 0" &
        index=`expr $index + 1`
        ssh node$i "/proj/dirr-PG0/tools/cloudlab-scripts/lustre/aging.py 100920 $index 9 0" &
        index=`expr $index + 1`
        ssh node$i "/proj/dirr-PG0/tools/cloudlab-scripts/lustre/aging.py 100920 $index 9 0" &
        index=`expr $index + 1`
    done
fi

# AGING
if [ "KILLAGING" = $3 ]; then
    for i in $(seq $start $end)
    do
        echo kill aging on client node$i
        ssh node$i "killall /usr/bin/python"
    done
fi