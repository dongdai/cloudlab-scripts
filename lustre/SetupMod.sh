#!/usr/bin/env bash

# As far as I know, Winsconsin Machines have two different network cards: enp6s0f0 and enp129s0f0

network1=`ifconfig|grep enp129s0f0`
if [ -n "$network1" ]; then
    echo "Server is having network as enp129s0f0"
    echo 'options lnet networks=tcp0(enp129s0f0)' > /etc/modprobe.d/lustre.conf
fi

network2=`ifconfig|grep enp6s0f0`
if [ -n "$network2" ]; then
    echo "Server is having network as enp6s0f0"
    echo 'options lnet networks=tcp0(enp6s0f0)' > /etc/modprobe.d/lustre.conf
fi

depmod -a
modprobe lustre
lctl set_param debug=+lfsck

# If there are some issues, just do this to reconfigure the network.
# sudo lctl network down
# sudo lustre_rmmod
# sudo modprobe -v lustre