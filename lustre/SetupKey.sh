#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 number-of-srv number-of-clt" >&2
    exit 1
fi

srv=`expr $1 - 1`
clt=`expr $2 - 1`

echo "Connect to all serves"
# Touch Every Server
for i in $(seq 0 $srv)
do
    ssh-keyscan node$i >> ~/.ssh/known_hosts
done

start=`expr $srv + 1`
end=`expr $start + $clt`
for i in $(seq $start $end)
do
    ssh-keyscan node$i >> ~/.ssh/known_hosts
done

echo "Setup Head Node"
cp /proj/dirr-PG0/rpms/id_rsa ~/.ssh/
ssh-agent bash
ssh-add
