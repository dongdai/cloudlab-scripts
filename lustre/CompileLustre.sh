#!/usr/bin/env bash
DIR=~/lustre-release
FILE=~/lustre-release/configure
MAKEFILE=~/lustre-release/Makefile

sudo yum -y install libyaml-devel
sudo yum -y install perf gawk
sudo yum -y install systemtap

cd ~

## Get Lustre 2.10.4
if [ ! -d "$DIR" ]; then
    git clone git://git.whamcloud.com/fs/lustre-release.git
    cd ~/lustre-release/
    git reset --hard 08876238836886bc6ad9b90c5ff50bdde87e2849
fi
cd ~/lustre-release/

if [ ! -f "$FILE" ]; then
    sh autogen.sh
fi

if [ ! -f "$MAKEFILE" ]; then
    ./configure --with-o2ib=no
fi

make rpms
