#!/usr/bin/env bash

if [ "$#" -ne 2 ]; then
    echo "Usage: $0 number-of-srv number-of-clt" >&2
    exit 1
fi

# c220g5 has /dev/sda as SSD and /dev/sdb as HDD
echo "Format MDS"
echo "===============================> Format Server MDS/MGS node$i"
sudo mkfs.lustre --fsname=lustre --mgs --mdt --index=0 --reformat /dev/sdb
sudo mkdir -p /lustre
sudo mount -t lustre /dev/sdb /lustre

echo "Format OSS"
srv=`expr $1 - 1`
for i in $(seq 1 $srv)
do
  echo "===============================> Format Server OSS node$i"
  ssh node$i "sudo mkfs.lustre --fsname=lustre --mgsnode=10.10.1.1@tcp0 --ost --index=$i --reformat /dev/sdb"
  ssh node$i "sudo mkdir -p /lustre"
  ssh node$i "sudo mount -t lustre /dev/sdb /lustre"
done

echo "Format Clients"
start=`expr $srv + 1`
clt=`expr $2 - 1`
end=`expr $start + $clt`
for i in $(seq $start $end)
do
  echo "===============================> Format Client node$i"
  ssh node$i "sudo mkdir -p /lustre"
  ssh node$i "sudo mount -t lustre 10.10.1.1@tcp0:/lustre /lustre"
  ssh node$i "sudo chown -R dirruncc:dirr-PG0 /lustre/"
done