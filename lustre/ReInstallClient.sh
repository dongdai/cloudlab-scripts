#!/usr/bin/env bash

echo "Install Lustre Client"
cd /users/dirruncc/lustre-release/
sudo yum -y install {kmod-lustre-client,kmod-lustre-client-tests,lustre-iokit,lustre-client,lustre-client-debuginfo,lustre-client-tests}-2.10.4-1.el7.x86_64.rpm
