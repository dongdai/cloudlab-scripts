#!/usr/bin/python

import os
import random
import math
import sys

if len(sys.argv) != 2:
    print ("Usage: ./net-analyze.py file-path")
    sys.exit()

file_path = sys.argv[1]
last_rx_ok = 0
last_tx_ok = 0

with open(file_path, 'r') as f:
    for line in f.readlines():        
        if not line.startswith('enp6s0f0'):
            continue
        network_array = line.split()
        rx_ok = int(network_array[2])
        tx_ok = int(network_array[6])

        if last_rx_ok == 0 or last_tx_ok == 0:
            last_rx_ok = rx_ok
            last_tx_ok = tx_ok
            continue  # skip the first line
        
        rx = rx_ok - last_rx_ok
        tx = tx_ok - last_tx_ok

        last_rx_ok = rx_ok
        last_tx_ok = tx_ok

        print (str(rx) + "\t" + str(tx))
        