#!/usr/bin/python

import os
import random
import math
import sys

if len(sys.argv) != 2:
    print ("Usage: ./disk-analyze.py file-path")
    sys.exit()

file_path = sys.argv[1]
first_record = 0

with open(file_path, 'r') as f:
    for line in f.readlines(): 
        if line.startswith('sdb'):
            io_array = line.split()
            read_kb = int(io_array[4])
            write_kb = int(io_array[5])
            if first_record == 0:
                first_record = 1
                continue
            print (str(read_kb) + "\t" + str(write_kb))