#!/usr/bin/env bash

echo "Remove Lustre components"
sudo yum -y remove kmod-lustre kmod-lustre-osd-ldiskfs kmod-lustre-tests lustre lustre-iokit lustre-osd-ldiskfs-mount lustre-tests lustre-debuginfo

echo "Install new Lustre components"
cd /users/dirruncc/lustre-release/
sudo yum -y localinstall {kmod-lustre-osd-ldiskfs,kmod-lustre,lustre,lustre-osd-ldiskfs-mount,lustre-iokit,lustre-tests,kmod-lustre-tests,lustre-debuginfo}-2.10.4-1.el7.x86_64.rpm

echo "Reboot to make new component work"
sudo reboot

